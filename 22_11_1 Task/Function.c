#define _CRT_SECURE_NO_WARNINGS
int Max(int a, int b) {
	if (a > b) {
		return a;
	}
	else {
		return b;
	}
}
int main() {
	int m = 0;
	int n = 0;
	int max = 0;
	sacnf("%d %d", &m, &n);
	max = Max(m, n);
	printf("%d", max);
	return 0;
}