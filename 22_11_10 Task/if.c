#define _CRT_SECURE_NO_WARNINGS
//三个数按从大到小排序
//#include <stdio.h>
//int main() {
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d %d %d", &a, &b, &c);
//	if (a >= b) {
//		if (b >= c) {
//			printf("%d %d %d", a, b, c);
//		}
//		else if (c >= a) {
//			printf("%d %d %d", c, a, b);
//		}
//		else {
//			printf("%d %d %d", a, c, b);
//		}
//	}
//	else if (b >= a) {
//		if (c >= b) {
//			printf("%d %d %d", c, b, a);
//		}
//		else if (c <= a) {
//			printf("%d %d %d", b, a, c);
//		}
//		else {
//			printf("%d %d %d", b, c, a);
//		}
//	}
//	return 0;
//}

//打印100-200之间的素数
//#include <stdio.h>
//#include <math.h>
//int Is_Prime(int n) {
//	int i = 0;
//	for (i = 2; i <= sqrt(n); i++) {
//		if (n % i == 0) {
//			return 0;
//		}
//	}
//	return 1;
//}
//int main() {
//	int i;
//	for (i = 100; i <= 200; i++) {
//		if (Is_Prime(i)) {
//			printf("%d  ", i);
//		}
//	}
//	return 0;
//}

//打印1000-2000之间的闰年
//#include<stdio.h>
//int Is_LeapYear(int year) {
//	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
//		return 1;
//	}
//		return 0;
//}
//int main() {
//	int i;
//	for (i = 1000; i <= 2000; i++) {
//		if (Is_LeapYear(i)) {
//			printf("%d  ", i);
//		}
//	}
//	return 0;
//}

//最大公约数（法一：暴力枚举法）
//int gcd(int a, int b) {
//	int tmp = 0;
//	int i = 0;
//	if (b > a) {
//		tmp = b;
//		b = a;
//		a = tmp;
//	}
//	for (i = b; i > 0; i--) {
//		if (a % i == 0 && b % i == 0) {
//			return i;
//		}
//	}
//}
//
//最大公约数(法二：辗转相除法)
//int gcd(int a, int b) {
//	int tmp = 0;
//	while (1) {
//		if (b > a) {
//			tmp = b;
//			b = a;
//			a = tmp;
//		}
//		if (a % b == 0) {
//			return b;
//		}
//		else {
//			a = a % b;
//		}
//	}
//}

//最大公约数（法三：更相减损法）
//int gcd(int a, int b) {
//	int tmp = 0;
//	while (1) {
//		if (b > a) {
//			tmp = b;
//			b = a;
//			a = tmp;
//		}
//		if (a - b == 0) {
//			return a;
//		}
//		else {
//			tmp = a - b;
//			a = b;
//			b = tmp;
//		}
//	}
//}
//#include<stdio.h>
//int main() {
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	printf("%d", gcd(a, b));
//	return 0;
//}