#include "Tgame.h"

//初始化棋盘
void Init_Board(char board[ROW][COL], int row, int col) {
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			board[i][j] = ' ';
		}
	}
}

//棋盘的输出
void Print_Board(char board[ROW][COL],int row,int col) {
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf(" %c ", board[i][j]);
			if (j < col - 1) {
				printf("|");
			}
		}
		printf("\n");
		if (i < row - 1) {
			for (j = 0; j < col; j++) {
				printf("---");
				if (j < col - 1) {
					printf("|");
				}
			}
			printf("\n");
		}
	}
}

//玩家输入
void Player_Input(char board[ROW][COL], int row, int col) {
	printf("玩家输入：>\n");
	int a = 0;
	int b = 0;
	while (1) {
		scanf("%d %d", &a, &b);
		if (1 <= a && a <= row && 1 <= b && b <= col) {
			if (board[a-1][b-1] == ' ') {
				board[a-1][b-1] = '*';
				break;
			}
			else {
				printf("该位置已被占用，请重新输入：\n");
			}
		}
		else {
			printf("坐标非法，请重新输入：\n");
		}
	}
}

//电脑输入
void Computer_Input(char board[ROW][COL], int row, int col) {
	printf("电脑下棋：>\n");
	while (1) {
		int x = rand() % row;
		int y = rand() % col;
		if (board[x][y] == ' ') {
			board[x][y] = '#';
			break;
		}
	}
}

//判断棋盘是否已满
static int Is_Full(char board[ROW][COL], int row, int col) {
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			if (board[i][j] == ' ') {
				return 0;
			}
		}
	}
	return 1;
}
//判断输赢
//P代表玩家赢
//C代表电脑赢
//D代表平局
//G代表还没下完棋
char Is_Win(char board[ROW][COL], int row, int col) {
	int i = 0;

	//判断行
	for(i = 0; i < row; i++) {
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0]!=' ') {
			return board[i][0];
		}
	}

	//判断列
	for (i = 0; i < col; i++) {
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ') {
			return board[0][i];
		}
	}

	//判断对角线
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ') {
		return board[0][0];
	}
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' ') {
		return board[0][2];
	}

	//判断是否平局
	if (Is_Full(board, row, col) == 1) {
		return 'D';
	}

	//未下完
	return 'G';
}