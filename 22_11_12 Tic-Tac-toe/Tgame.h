#pragma once
#define _CRT_SECURE_NO_WARNINGS

#define ROW 3
#define COL 3

#include <stdio.h>
#include <stdlib.h>
#include<time.h>

//初始化棋盘
void Init_Board(char board[ROW][COL],int row,int col);

//输出棋盘
void Print_Board(char board[ROW][COL],int row,int col);

//玩家输入
void Player_Input(char board[ROW][COL], int row, int col);

//电脑输入
void Computer_Input(char board[ROW][COL], int row, int col);

//判断输赢
//P代表玩家赢
//C代表电脑赢
//D代表平局
char Is_Win(char board[ROW][COL], int row, int col);



