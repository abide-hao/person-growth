#include "Tgame.h"
void menu() {
	printf("**************************\n");
	printf("********1、Play    *******\n");
	printf("********0、Exit    *******\n");
	printf("**************************\n");
}

void game() {
	char board[ROW][COL];
	char ret = 0;
	Init_Board(board,ROW,COL);
	Print_Board(board,ROW,COL);
	while (1) {
		Player_Input(board, ROW, COL);
		Print_Board(board, ROW, COL);
		ret = Is_Win(board, ROW, COL);
		if (ret != 'G') {
			break;
		}
		Computer_Input(board, ROW, COL);
		Print_Board(board, ROW, COL); ret = Is_Win(board, ROW, COL);
		if (ret != 'G') {
			break;
		}
	}
	if (ret == '*') {
		printf("玩家赢得比赛\n");
	}
	else if (ret == '#') {
		printf("电脑赢得比赛\n");
	}
	else if (ret == 'D') {
		printf("平局\n");
	}
}
void test1() {
	int input = 0;
	do {
		menu();
		printf("请选择：\n");
		scanf("%d", &input);
		switch (input) {
		case 1:
			game();
			//printf("game\n");
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("非法输入，请重新选择：");
			break;
		}
	} while (input);
}

int main() {
	srand((unsigned int)time(NULL));
	test1();
	return 0;
}