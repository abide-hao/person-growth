#define _CRT_SECURE_NO_WARNINGS
#include"Mgame.h"

//初始化棋盘
void Init_Board(char board[ROWS][COLS], int rows, int cols,char set) {
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			board[i][j] = set;
		}
	}
}

//打印棋盘
void Print_Board(char show[ROWS][COLS], int row, int col) {
	int i = 0;
	int j = 0;
	printf("---------扫雷----------\n");
	//出现序号
	for (i = 0; i <= col; i++) {
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++) {
		printf("%d ", i);
		for (j = 1; j <= col; j++) {
			printf("%c ", show[i][j]);
		}
		printf("\n");
	}
	printf("---------扫雷----------\n");
}

//布置雷
void Setmine(char mine[ROWS][COLS], int row, int col) {
	int count = EASY_COUNT;
	while (count) {
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (mine[x][y] == '0') {
			mine[x][y] = '1';
			count--;
		}
	}
}

//周围雷的个数
int Mine_Number(char mine[ROWS][COLS],int x,int y) {
	return (mine[x - 1][y - 1] + mine[x - 1][y] + mine[x - 1][y + 1]
		+ mine[x][y - 1] + mine[x][y + 1]
		+ mine[x + 1][y - 1] + mine[x + 1][y] + mine[x + 1][y + 1] - 8 * '0');
}
//排雷
void Findmine(char mine[ROWS][COLS],char show[ROWS][COLS], int row, int col) {
	int x = 0;
	int y = 0;
	int win = 0;
	while (win < (row * col - EASY_COUNT)) {
		printf("请选择需要排查的雷坐标：\n");
		scanf("%d %d", &x, &y);
		if (1 <= x && x <= row && 1 <= y && y <= col) {
			if (show[x][y] != '*') {
				printf("坐标已被占用，请重新输入：\n");
				continue;
			}
			if (mine[x][y] == '1') {
				printf("你被炸死了!!!\n");
				Print_Board(show, ROW, COL);
				break;
			}
			else {
				int n = Mine_Number(mine, x, y);
				show[x][y] = n + '0';
				Print_Board(show, ROW, COL);
				win++;
			}
		}
		else {
			printf("坐标非法，请重新输入：\n");
		}
	}
	if (win == (ROW * COL - EASY_COUNT)) {
		printf("你赢了\n");
		Print_Board(mine, ROW, COL);
	}
}