#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define EASY_COUNT 60

//初始化棋盘
void Init_Board(char board[ROWS][COLS], int rows, int cols, char set);

//打印棋盘
void Print_Board(char show[ROWS][COLS], int row, int col);

//布置雷
void Setmine(char mine[ROWS][COLS], int row, int col);

//周围雷的个数
int Mine_Number(char mine[ROWS][COLS], int x, int y);

//排雷
void Findmine(char mine[ROWS][COLS],char show[ROWS][COLS], int row, int col);