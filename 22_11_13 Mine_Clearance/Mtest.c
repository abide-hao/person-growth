#define _CRT_SECURE_NO_WARNINGS
#include "Mgame.h"
void menu() {
	printf("**********************\n");
	printf("****** 1、Play  ******\n");
	printf("****** 0、Exit  ******\n");
	printf("**********************\n");

}
void game() {
	char mine[ROWS][COLS] = { 0 };
	char show[ROWS][COLS] = { 0 };

	//初始化棋盘
	Init_Board(mine, ROWS, COLS, '0');
	Init_Board(show, ROWS, COLS, '*');

	//打印棋盘
	Print_Board(show, ROW, COL);

	//布置雷
	Setmine(mine, ROW, COL);

	//排雷
	Findmine(mine, show, ROW, COL);
}

void test1() {
	int input = 0;
	srand((unsigned int)time(NULL));
	do {
		menu();
		printf("请选择：\n");
		scanf("%d", &input);
		switch (input) {
		case 1:
			game();
			/*printf("OK\n");*/
			break;
		case 0:
			printf("游戏结束\n");
			break;
		default:
			printf("输入非法，请重新输入：\n");
			break;
		}
	} while (input);
}

int main() {
	test1();
	return 0;
}