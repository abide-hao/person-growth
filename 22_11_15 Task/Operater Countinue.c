#define _CRT_SECURE_NO_WARNINGS

//统计二进制中的1的个数
//#include <stdio.h>
//int NumberOf1(int n) {
//    // write code here
//    int cnt = 0;
//    while (n != 0) {
//        cnt++;
//        n = n & (n - 1);
//    }
//    return cnt;
//}
//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    printf("%d", NumberOf1(n));
//    return 0;
//}

//

//求两个数二进制中不同位的个数
//#include <stdio.h>
//int NumberOf3(int m, int n) {
//	int num = m ^ n;
//	int cnt = 0;
//	while (num != 0) {
//		cnt++;
//		num = num & (num - 1);
//	}
//	return cnt;
//}
//int main() {
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//	printf("%d", NumberOf3(m, n));
//	return 0;
//}

//牛客网 BC117-小乐乐走台阶
//方法一：递归
// int Fs(int n) {
//     if (n <= 2) {
//         return n;
//     } else {
//         return Fs(n - 1) + Fs(n - 2);
//     }
// }
//方法二：循环+数组
//int Fs(int n) {
//    int i = 0;
//    int a[100] = { 0 };
//    a[0] = 1;
//    a[1] = 1;
//    for (i = 2; i <= n; i++) {
//        a[i] = a[i - 1] + a[i - 2];
//    }
//    return a[n];
//}
//#include <stdio.h>
//int main() {
//	int n = 0;
//	int sum = 0;
//	scanf("%d", &n);
//	sum = Fs(n);
//	printf("%d", sum);
//	return 0;
//}

//牛客网 BC54-获得月份天数
//#include <stdio.h>
//int Is_LeapYear(int year) {
//	if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
//		return 1;
//	}
//	return 0;
//}
//void Dotm(int year,int month) {
//	switch (month) {
//	case 2: {
//		if (Is_LeapYear(year)) {
//			printf("29");
//		}
//		else {
//			printf("28");
//		}
//		break;
//	}
//	case 1:case 3:case 5:case 7:case 8:case 10:case 12:
//		printf("31");
//		break;
//	default:
//		printf("30");
//		break;
//	}
//}
//int main() {
//	int year = 0;
//	int month = 0;
//	scanf("%d %d", &year, &month);
//	Dotm(year, month);
//	return 0;
//}

//打印整数二进制的奇数位和偶数位
//#include <stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	printf("偶数位为：");
//	for (i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	printf("\n");
//	printf("奇数位为：");
//	for (i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	return 0;
//}