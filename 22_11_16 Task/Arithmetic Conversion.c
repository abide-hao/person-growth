#define _CRT_SECURE_NO_WARNINGS
//矩阵计算
//#include <stdio.h>
//int main() {
//	int row = 0;
//	int col = 0;
//	int a[10][10];
//	int i = 0;
//	int j = 0;
//	int sum = 0;
//	scanf("%d %d", &row, &col);
//	for (i = 0; i < row; i++) {
//		for (j = 0; j < col; j++) {
//			scanf("%d", &a[i][j]);
//			if (a[i][j] > 0) {
//				sum += a[i][j];
//			}
//		}
//	}
//	printf("%d", sum);
//	return 0;
//}

//牛客 BC111-小乐乐与进制转换
//#include<stdio.h>
//int main() {
//	int n = 0;
//	int a[1000];
//	int cnt = 0;
//	scanf("%d", &n);
//	do {
//		a[cnt] = n % 6;
//		n /= 6;
//		cnt++;
//	} while (n != 0);
//	while (cnt > 0) {
//		printf("%d", a[cnt - 1]);
//		cnt--;
//	}
//	return 0;
//}

//牛客 BC107-矩阵转置
//#include <stdio.h>
//int main() {
//	int row = 0;
//	int col = 0;
//	int i = 0;
//	int j = 0;
//	int a[10][10];
//	scanf("%d %d", &row, &col);
//	for (i = 0; i < row; i++) {
//		for (j = 0; j < col; j++) {
//			scanf("%d", &a[i][j]);
//		}
//	}
//	for (i = 0; i < col; i++) {
//		for (j = 0; j < row; j++) {
//			printf("%d ", a[j][i]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//牛客 BC98-序列中删除指定数字
void Move(int a[], int i, int n) {
	while (i < n-1) {
		a[i] = a[i + 1];
		i++;
	}
}
#include <stdio.h>
int main() {
	int i = 0;
	int n = 0;
	int num = 0;
	int a[51];
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	scanf("%d", &num);
	for (i = 0; i < n; i++) {
		if (a[i] == num) {
			Move(a, i, n);
			n--;
			i--;
		}
	}
	for (i = 0; i < n; i++) {
		printf("%d ", a[i]);
	}
	return 0;
}