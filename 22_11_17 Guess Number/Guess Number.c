#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define Max_Size 7
void menu() {
	printf("************************\n");
	printf("*******  1、PLAY  ******\n");
	printf("*******  0、EXIT  ******\n");
	printf("************************\n");
}

//游戏主体
void game() {
	int ret = rand() % 100 + 1;
	int num = 0;
	int count = 0;
	printf("请输入你猜的数字：\n");
	while (1) {
		scanf("%d", &num);
		count++;
		if (num > ret) {
			printf("猜大了");
		}
		else if (num < ret) {
			printf("猜小了");
		}
		else {
			printf("恭喜你，猜对了！！！\n");
			break;
		}
		if (count >= Max_Size) {
			printf("超出了游戏次数，游戏结束！！！\n");
			break;
		}
		printf("，请重新猜：\n");
	}
}
int main() {
	srand((unsigned int)time(NULL));
	int input = 0;
	do {
		printf("请选择菜单：\n");
		scanf("%d", &input);
		switch (input) {
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("非法输入，请重新选择：\n");
			break;
		}
	} while (input);
	return 0;
}