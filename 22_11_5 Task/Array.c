#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//void ArrayExchange(int arr1[], int arr2[],int n) {
//	int tmp = 0;
//	int i = 0;
//	for (i = 0; i < n; i++) {
//		tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//}
//int main(){
//	int arr1[5]={0};
//	int arr2[5]={0};
//	int i = 0;
//	int n = 0;
//	scanf("%d", &n);
//	for (i = 0; i < n; i++) {
//		scanf("%d", &arr1[i]);
//	}
//	for (i = 0; i < n; i++) {
//		scanf("%d", &arr2[i]);
//	}
//	//交换前
//	printf("交换前：\n");
//	for (i = 0; i < n; i++) {
//		printf("arr1[%d]=%d  arr2[%d]=%d\n", i, arr1[i], i, arr2[i]);
//	}
//	ArrayExchange(arr1, arr2, n);
//	//交换后
//	printf("交换后：\n");
//	for (i = 0; i < n; i++) {
//		printf("arr1[%d]=%d  arr2[%d]=%d\n", i, arr1[i], i, arr2[i]);
//	}
//	return 0;
//}
//void init(int* arr,int len) {
//	int i = 0;
//	for (i = 0; i < len; i++) {
//		*(arr + i) = 0;
//	}
//}
//void print(int* arr, int len) {
//	int i = 0;
//	for (i = 0; i < len; i++) {
//		printf("%d   ", *(arr + i));
//	}
//}
//void reserve(int* arr, int len) {
//	int* temp = arr;
//	int i = 0;
//	while (i < len) {
//		temp = *(arr + i);
//		*(arr + i) = *(arr + len-1);
//		*(arr + len-1) = temp;
//		i++;
//		len--;
//	}
//}
//#include<stdio.h>
//int main(){
//	int arr[100];
//	int i = 0;
//	int n = 0;
//	printf("请输入数组中的元素个数：");
//	scanf("%d", &n);
//	init(&arr, n);
//	printf("数组初始化为：");
//	print(&arr, n);
//	printf("\n");
//	printf("请输入%d个数组元素：", n);
//	for (i = 0; i < n; i++) {
//		scanf("%d",& arr[i]);
//	}
//	printf("原数组为：");
//	print(&arr, n);
//	printf("\n");
//	reserve(&arr, n);
//	printf("逆置后的数组为：");
//	print(&arr, n);
//	return 0;
//}
void Bubble_Sort(int a[], int len) {
	int i = 0;
	int j = 0;
	int tmp = 0;
	for (i = 0; i < len - 1; i++) {
		for (j = 0; j < len - 1 - i; j++) {
			if (a[j] > a[j + 1]) {
				tmp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = tmp;
			}
		}
	}
}
#include <stdio.h>
int main() {
	int a[100];
	int n;
	int i;
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	Bubble_Sort(a, n);
	for (i = 0; i < n; i++) {
		printf("%d ", a[i]);
	}
	return 0;
}