#define _CRT_SECURE_NO_WARNINGS
int Max(int a, int b) {
	if (a > b) {
		return a;
	}
	else {
		return b;
	}
}
#include <stdio.h>
int main() {
	int m = 0;
	int n = 0;
	int max = 0;
	scanf("%d %d", &m, &n);
	max = Max(m, n);
	printf("max = %d", max);
	return 0;
}
