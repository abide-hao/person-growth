#define _CRT_SECURE_NO_WARNINGS
//乘法口诀表
//#include<stdio.h>
//void Print(int a) {
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= a; i++) {
//		for (j = 1; j <= i; j++) {
//			printf("%d * %d = %-3d ", j, i, i * j);
//		}
//		printf("\n");
//	}
//}
//int main() {
//	int i = 0;
//	int j = 0;
//	int m = 0;
//	scanf("%d", &m);
//	Print(m);
//	return 0;
//}

//交换两个整数
//#include<stdio.h>
//void swap(int* a, int* b) {
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//int main() {
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//	printf("交换前：%d %d\n", m, n);
//	swap(&m, &n);
//	printf("交换后：%d %d", m, n);
//	return 0;
//}

//判断闰年
//#include <stdio.h>
//int IsLeapYear(int year) {
//	if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
//		return 1;
//	}
//	else {
//		return -1;
//	}
//}
//int main() {
//	int year;
//	scanf("%d", &year);
//	if (IsLeapYear(year) == 1) {
//		printf("%d 年是闰年", year);
//	}
//	else {
//		printf("%d 年不是闰年", year);
//	}
//	return 0;
//}

//判断素数
#include <stdio.h>
#include <math.h>
int IsPNumber(int a) {
	int i = 0;
	if (a == 1) {
		return -1;
	}
	for (i = 2; i <= sqrt(a); i++) {
		if (a % i == 0) {
			return -1;
		}
	}
	return 1;
}
int main() {
	int m = 0;
	int n = 0;
	int i = 0;
	scanf("%d %d", &m, &n);
	for (i = m; i <= n; i++) {
		if (IsPNumber(i) == 1) {
			printf("%-5d", i);
		}
	}
	return 0;
}