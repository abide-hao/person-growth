#define _CRT_SECURE_NO_WARNINGS
//递归实现n的k次方
//#include <stdio.h>
//double fun(int n,int k) {
//	if (k == 0) {
//		return 1;
//	}
//	else if (k > 0) {
//		return fun(n, k-1) * n;
//	}
//	else {
//		return 1.0/ fun(n, -k);
//	}
//}
//int main() {
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	printf("%lf", fun(n, k));
//	return 0;
//}

//递归实现计算一个数的每位之和
//int sum(int n)
//{
//    if (n / 10 != 0) {
//        return (n % 10 + sum(n / 10));
//    }
//    else if (n / 10 == 0) {
//        return n;
//    }
//}
//#include <stdio.h>
//int main() {
//    int n;
//    scanf("%d", &n);
//    printf("%d", sum(n));
//}

//用函数模拟strlen函数（递归）
//int ST(char* s) {
//	if (*s != '\0') {
//		return ST(s+1)+1;
//	}
//	else {
//		return 0;
//	}
//}
//#include <stdio.h>
//int main() {
//	char a[100] = { 0 };
//	gets(a);
//	printf("%d", ST(&a));
//	return 0;
//}

//用函数模拟strlen函数（非递归）
//int ST(char* s) {
//	int cnt = 0;
//	while (*s != '\0') {
//		s++;
//		cnt++;
//	}
//	return cnt;
//}
//#include <stdio.h>
//int main() {
//	char a[100] = { 0 };
//	gets(a);
//	printf("%d", ST(&a));
//	return 0;
//}

//打印一个数的每一位(递归)
#include <stdio.h>
void Print(int n) {
	if (n / 10 != 0) {
		        Print(n/10);
	 }
	printf("%d ", n%10);  
}
int main() {
	int n = 0;
	scanf("%d", &n);
	Print(n);
	return 0;
}