#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"
void SLInit(SL* ps) {

	ps->capacity = 0;
	ps->size = 0;
}

void SLPrint(SL* ps) {
	int i = 0;
	for (i = 0; i < ps->size; i++) {
		printf("%d  ", ps->a[i]);
	}
}

void SLDestory(SL* ps) {
	free(ps->a);
	ps->size = ps->capacity = 0;
}

void SLCapacityCheck(SL* ps) {
	int newcapacity = 0;
	if (ps->size == ps->capacity) {
		newcapacity = ps->capacity == 0 ? 4 : (2 * ps->capacity);
		SLDataType* temp = (SLDataType*)malloc(newcapacity * sizeof(SLDataType));
		if (temp == NULL) {
			printf("realloc fail\n");
			exit(-1);
		}
		ps->capacity = newcapacity;
		ps->a = temp;
	}
}

void SLPushBack(SL* ps, SLDataType x) {
	SLCapacityCheck(ps);
	ps->a[ps->size] = x;
	ps->size++;
}

void SLPopBack(SL* ps) {
	if (ps->size > 0) {
		ps->size--;
	}
	else {
		printf("Data Lack!!!\n");
		return;
	}
}

void SLPushFront(SL* ps, SLDataType x){
	SLCapacityCheck(ps);
	int end = ps->size;
	while (end > 0) {
		ps->a[end] = ps->a[end - 1];
		end--;
		}
	ps->a[0] = x;
	ps->size++;
}

void SLPopFront(SL* ps) {
	if (ps->size > 0) {
		int front = 0;
		while (front < ps->size - 1) {
			ps->a[front] = ps->a[front + 1];
			front++;
		}
	}
	else {
		printf("Data Lack!!!\n");
		return;
	}
	ps->size--;
}

int SLFind(SL* ps, SLDataType x) {
	int i = 0;
	for (i = 0; i < ps->size; i++) {
		if (ps->a[i] == x) {
			return i + 1;
		}
	}
	printf("Not Find\n");
	return -1;
}

void SLErase(SL* ps, int pos) {
	if (pos<1 || pos>ps->size||ps->size<=0) {
		printf("pos invalid\n");
		return;
	}
	int front = pos;
	while (front <= ps->size) {
		ps->a[front - 1] = ps->a[front];
		front++;
	}
	ps->size--;
}

void SLInsret(SL* ps, int pos, SLDataType x) {
	if (pos<1 || pos>ps->size + 1) {
		printf("pos invalid\n");
		return;
	}
	SLCapacityCheck(ps);
	int end = ps->size;
	while (end > pos - 1) {
		ps->a[end] = ps->a[end - 1];
		end--;
	}
	ps->a[pos - 1] = x;
	ps->size++;
}