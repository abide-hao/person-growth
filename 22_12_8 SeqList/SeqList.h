#pragma once
#include <stdio.h>
#include <stdlib.h>

typedef int SLDataType;

typedef struct SeqList {
	SLDataType* a;
	int size;
	int capacity;
}SL;

//初始化顺序表
void SLInit(SL* ps);

//顺序表的输出
void SLPrint(SL* ps);

//释放内存
void SLDestory(SL* ps);

//容量检查
void SLCapacityCheck(SL* ps);

//尾插
void SLPushBack(SL* ps, SLDataType x);

//尾删
void SLPopBack(SL* ps);

//头插
void SLPushFront(SL* ps, SLDataType x);

//头删
void SLPopFront(SL* ps);

//指定元素查找
int SLFind(SL* ps, SLDataType x);

//指定位置元素删除
void SLErase(SL* ps, int pos);

//指定位置元素增加
void SLInsret(SL* ps, int pos, SLDataType x);