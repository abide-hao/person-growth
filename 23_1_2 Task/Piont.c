#define _CRT_SECURE_NO_WARNINGS
//使用指针打印数组内容
//void Print(int* a, int n) {
//	int i = 0;
//	for (i = 0; i < n; i++) {
//		printf("%d ", *(a + i));
//	}
//}
//#include<stdio.h>
//int main() {
//	int a[100] = { 0 };
//	int n = 0;
//	int i = 0;
//	scanf("%d", &n);
//	for (i = 0; i < n; i++) {
//		scanf("%d", &a[i]);
//	}
//	Print(&a, n);
//	return 0;
//}

//字符串逆序
//#include <stdio.h>
//#include <string.h>
//int main() {
//    char a[10000] = { '0'};
//    int len = 0;
//    int i = 0;
//    gets(a);
//    len = strlen(a);
//    for (i = 0; i < len; i++) {
//        printf("%c", *(a + len - i - 1));
//    }
//    return 0;
//}

//打印菱形
//#include <stdio.h>
//int main() {
//	int n = 0;
//	int i = 0;
//	int j = 0;
//	scanf("%d", &n);
//	for (i = 1; i <= n / 2; i++) {
//		for (j = 0; j < n/2 - i; j++) {
//			printf(" ");
//		}
//		for (j = 0; j < 2 * i - 1; j++) {
//			printf("*");
//		}
//		printf("\n");
//	}
//	for (i = 1; i <= n / 2; i++) {
//		for (j = 0; j < i; j++) {
//			printf(" ");
//		}
//		for (j = 0; j < 2 * (n/2-1-i)+1; j++) {
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//打印水仙花数
//#include <stdio.h>
//#include <math.h>
//int Digit(int n) {
//	int cnt = 1;
//	while (n / 10 != 0) {
//		cnt++;
//		n /= 10;
//	}
//	return cnt;
//}
//int Is_Narn(int n) {
//	int sum = 0;
//	int num = n;
//	int len = Digit(n);
//	while (n / 10 != 0) {
//		sum += pow((n % 10),len);
//		n /= 10;
//	}
//	sum += pow(n,len);
//	if (sum == num) {
//		return 1;
//	}
//	else {
//		return -1;
//	}
//}
//
//int main() {
//	int i = 0;
//	for (i = 0; i <= 100000; i++) {
//		if (Is_Narn(i) == 1) {
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//计算求和
//#include <stdio.h>
//#include <math.h>
//int main() {
//	int n = 0;
//	int i = 0;
//	int sum = 0;
//	int num = 0;
//	scanf("%d", &n);
//	for (i = 0; i < 5; i++) {
//		num = (num + n * pow(10, i));
//		sum += num;
//	}
//	printf("%d", sum);
//	return 0;
//}